using UnityEngine;

public class Hamster : MonoBehaviour
{
    public Variables Variables;
    public HamsterWeight HamsterWeight;
    Rigidbody2D _RB2D;
    Animator _Animator;
    readonly int _MovementSpeed = Animator.StringToHash("Speed");
    readonly int _IsFat = Animator.StringToHash("Fat");
    float _CurrentSpeed = 1f;
    float _WeightFactor = 1f;
    public float _TimeBeforeWeightLoss = 1f;
    float _CurrentTimeLoss;
    public float TimeForBuff = 2f;
    float _CurrentTimeBuff;
    public float TimeForDeBuff = 5f;
    float _CurrentTimeDeBuff;

    void
    Awake()
    {
        _RB2D = GetComponent<Rigidbody2D>();
        _Animator = GetComponent<Animator>();
        HamsterWeight.OverWeight += OnHamsterOverWeight;
        HamsterWeight.UnderWeight += OnHamsterUnderWeight;
    }

    void 
    OnHamsterUnderWeight(object sender, System.EventArgs e)
    {
        _Animator.SetTrigger(_IsFat);
    }

    void 
    OnHamsterOverWeight(object sender, System.EventArgs e)
    {
        _Animator.SetTrigger(_IsFat);
    }

    void 
    Update()
    {
        _Animator.SetFloat(_MovementSpeed, _CurrentSpeed * _WeightFactor);
    }

    void
    FixedUpdate()
    {
        if (HamsterWeight.Value > 40 && HamsterWeight.Value < 60)
        {
            _CurrentTimeDeBuff = 0f;
            if (_CurrentTimeBuff < TimeForBuff)
            {
                _CurrentTimeBuff += Time.deltaTime;
            }
            else
            {
                _CurrentTimeBuff = 0f;
                _CurrentSpeed += .015f;
            }
        }
        else
        {
            _CurrentTimeBuff = 0f;
            if (_CurrentTimeDeBuff < TimeForDeBuff)
            {
                _CurrentTimeDeBuff += Time.deltaTime;
            }
            else
            {
                _CurrentTimeDeBuff = 0f;
                _CurrentSpeed -= .01f;
                _CurrentSpeed = _CurrentSpeed >= 1f ? _CurrentSpeed : 1f;
            }
        }
        if (_CurrentTimeLoss < _TimeBeforeWeightLoss)
        {
            _CurrentTimeLoss += Time.deltaTime;
        }
        else
        {
            _CurrentTimeLoss = 0f;
            HamsterWeight.Value -= HamsterWeight.LossRate;
        }
        _WeightFactor = Mathf.Abs(Mathf.Abs((float)(HamsterWeight.Value - HamsterWeight.PERFECT_WEIGHT) / HamsterWeight.PERFECT_WEIGHT) - 1.1f);
        _RB2D.velocity = FindBestFood() * _CurrentSpeed * _WeightFactor + Vector2.down * Variables.WheelSpeed;
    }

    Vector2
    FindBestFood()
    {
        Vector2 direction = Vector2.up;
        float bestgainrate = 0f;

        for (int i = 0; i < Variables.Foods.Length; i++)
        {
            if (Variables.Foods[i] != null && Variables.Foods[i].transform.position.y > transform.position.y)
            {
                float gainrate = Variables.Foods[i].WeightGain / (Variables.Foods[i].transform.position - transform.position).magnitude;
                if (gainrate > bestgainrate)
                {
                    direction = (Variables.Foods[i].transform.position - transform.position).normalized;
                    bestgainrate = gainrate;
                }
            }    
        }

        return direction;
    }
}
