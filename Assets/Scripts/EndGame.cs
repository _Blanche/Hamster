﻿using UnityEngine;

public class EndGame : MonoBehaviour
{
    public Variables Variables;
    public EndMenu EndMenu;

    void 
    OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            Destroy(collision.gameObject);
            Variables.Clock.Stop();
            EndMenu.gameObject.SetActive(true);
            EndMenu.Open();
        }
    }
}