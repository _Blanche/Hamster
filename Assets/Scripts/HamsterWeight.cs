﻿using UnityEngine;

[CreateAssetMenu(fileName = "HamsterWeight", menuName = "Hamster/HamsterWeight")]
public class HamsterWeight : ScriptableObject
{
    public event System.EventHandler Changed;
    public event System.EventHandler OverWeight;
    public event System.EventHandler UnderWeight;

    int _Value = 50;
    public int Value
    {
        get { return _Value; }
        set
        {
            if (value >= _MAX_WEIGHT)
            {
                OverWeight?.Invoke(this, System.EventArgs.Empty);
            }
            else if (value <= 0)
            {
                UnderWeight?.Invoke(this, System.EventArgs.Empty);
            }
            _Value = value;
            Changed?.Invoke(this, System.EventArgs.Empty);
        }
    }
    int _MAX_WEIGHT = 100;
    public int PERFECT_WEIGHT = 50;
    public int LossRate = 2;

    public void
    Reset()
    {
        _Value = PERFECT_WEIGHT;
        LossRate = 2;
    }

    void
    OnDisable()
    {
        Reset();
    }
}
