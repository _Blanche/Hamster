﻿using UnityEngine;

[CreateAssetMenu(fileName ="Variables", menuName ="Hamster/Variables")]
public class Variables : ScriptableObject
{
    public event System.EventHandler FSSChanged;
    public event System.EventHandler FMSChanged;
    public event System.EventHandler FLSChanged;

    int _FoodSStack = 10;
    public int FoodSStack
    {
        get { return _FoodSStack; }
        set
        {
            _FoodSStack = value;
            FSSChanged?.Invoke(this, System.EventArgs.Empty);
        }
    }
    int _FoodMStack = 5;
    public int FoodMStack
    {
        get { return _FoodMStack; }
        set
        {
            _FoodMStack = value;
            FMSChanged?.Invoke(this, System.EventArgs.Empty);
        }
    }
    int _FoodLStack = 3;
    public int FoodLStack
    {
        get { return _FoodLStack; }
        set
        {
            _FoodLStack = value;
            FLSChanged?.Invoke(this, System.EventArgs.Empty);
        }
    }

    public float WheelSpeed = 1f;
    public Food[] Foods;
    public Clock Clock;

    public void 
    Reset()
    {
        WheelSpeed = 1f;
        _FoodSStack = 10;
        _FoodMStack = 5;
        _FoodLStack = 3;
    }

    void
    OnDisable()
    {
        Reset();
        Clock = null;
        Foods = null;
    }
}
