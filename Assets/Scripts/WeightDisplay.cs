﻿using UnityEngine;
using UnityEngine.UI;

public class WeightDisplay : MonoBehaviour
{
    public HamsterWeight HamsterWeight;
    public Color32 Good;
    public Color32 Worrying;
    public Color32 Bad;
    public Image SliderFill;
    Slider _WeightSlider;
    bool _unsubscribed;

    void
    Awake()
    {
        _WeightSlider = GetComponent<Slider>();
        _WeightSlider.value = HamsterWeight.Value;
        SliderFill.color = Good;
        HamsterWeight.Changed += OnHamsterWeightChanged;
        HamsterWeight.OverWeight += OnHamsterOverWeight;
        HamsterWeight.UnderWeight += OnHamsterUnderWeight;
    }

    void
    OnHamsterUnderWeight(object sender, System.EventArgs e)
    {
        HamsterWeight.Changed -= OnHamsterWeightChanged;
        _WeightSlider.value = 0;
        _unsubscribed = true;
    }

    void
    OnHamsterOverWeight(object sender, System.EventArgs e)
    {
        HamsterWeight.Changed -= OnHamsterWeightChanged;
        _WeightSlider.value = 100;
        _unsubscribed = true;
    }

    void
    OnHamsterWeightChanged(object sender, System.EventArgs e)
    {
        if (HamsterWeight.Value < 20 || HamsterWeight.Value > 80)
        {
            SliderFill.color = Bad;
        }
        else if (HamsterWeight.Value < 40 || HamsterWeight.Value > 60)
        {
            SliderFill.color = Worrying;
        }
        else
        {
            SliderFill.color = Good;
        }
        _WeightSlider.value = HamsterWeight.Value;
    }

    public void
    Restart()
    {
        _WeightSlider.value = HamsterWeight.Value;
        SliderFill.color = Good;
        if (_unsubscribed)
        {
            _unsubscribed = false;
            HamsterWeight.Changed += OnHamsterWeightChanged;
        }
    }
}
