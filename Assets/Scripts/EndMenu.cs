﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndMenu : MonoBehaviour
{
    public Variables Variables;
    const string TimeElapsedTextBase = "NOM TIME :    ";
    const string BestTimeTextBase = "NOMEST TIME :    ";

    bool _isNewRecord;
    Coroutine _CurrentLoop;

    public Text TimeElapsed;
    public Text BestTime;
    public Text NewRecord;
    //public RectTransform RotatingBeer01;
    //public RectTransform RotatingBeer02;

    public void
    Open()
    {
        TimeElapsed.text = TimeElapsedTextBase + FormatTime(Variables.Clock.TimeElapsed);
        BestTime.text = BestTimeTextBase + FormatTime(Variables.Clock.BestTime);

        if (Variables.Clock.isNewBestTime)
        {
            _isNewRecord = true;
            NewRecord.gameObject.SetActive(true);
            //RotatingBeer01.gameObject.SetActive(true);
            //RotatingBeer02.gameObject.SetActive(true);
            _CurrentLoop = StartCoroutine(ChangeNewRecordColor());
        }
    }

    //void
    //Update()
    //{
    //    if (_isNewRecord)
    //    {
    //        RotatingBeer01.Rotate(0f, 0f, 10f);
    //        RotatingBeer02.Rotate(0f, 0f, -10f);
    //    }
    //}

    public void
    Restart()
    {
        Close();
    }

    public void
    Close()
    {
        if (_isNewRecord)
        {
            StopCoroutine(_CurrentLoop);
            _isNewRecord = false;
            NewRecord.gameObject.SetActive(false);
            //RotatingBeer01.gameObject.SetActive(false);
            //RotatingBeer02.gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }

    System.Collections.IEnumerator
    ChangeNewRecordColor()
    {
        yield return new WaitForSecondsRealtime(0.25f);
        NewRecord.color = Random.ColorHSV(0.1f, 1f, 0.8f, 1f, 0.8f, 1f, 1f, 1f);
        _CurrentLoop = StartCoroutine(ChangeNewRecordColor());
    }

    string
    FormatTime(float time)
    {
        return string.Format("{0:00}:{1:00}:{2:000}", (int)time / 60, (int)time % 60, (int)(time * 1000) % 1000);
    }
}
