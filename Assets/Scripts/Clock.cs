﻿public class Clock
{
    public float BestTime { get; private set; }
    public float TimeElapsed { get; private set; }
    public bool isRunning { get; private set; }
    public bool isNewBestTime { get; private set; }

    public
    Clock()
    {
        BestTime = 0f;
        TimeElapsed = 0f;
        isRunning = false;
        isNewBestTime = false;
    }

    public void
    Update()
    {
        if (isRunning)
        {
            TimeElapsed += UnityEngine.Time.deltaTime;
        }
    }

    public void
    Start()
    {
        TimeElapsed = 0f;
        isRunning = true;
    }

    public void
    Stop()
    {
        isRunning = false;
        isNewBestTime = CheckNewBestTime();
    }

    public void
    Reset()
    {
        isRunning = false;
        isNewBestTime = false;
        TimeElapsed = 0f;
    }

    bool
    CheckNewBestTime()
    {
        if (BestTime < TimeElapsed)
        {
            BestTime = TimeElapsed;
            return true;
        }
        return false;
    }
}
