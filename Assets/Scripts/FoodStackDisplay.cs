﻿using UnityEngine;
using UnityEngine.UI;

public class FoodStackDisplay : MonoBehaviour
{
    public Variables Variables;
    public bool SStack;
    public bool MStack;
    public bool LStack;
    public Text _Text;
    Button _Button;
    void
    Awake()
    {
        _Button = GetComponent<Button>();
        _Text.text = (SStack ? Variables.FoodSStack : MStack ? Variables.FoodMStack : Variables.FoodLStack).ToString();
        if (SStack)
        {
            Variables.FSSChanged += OnFSSChanged;
        }
        if (MStack)
        {
            Variables.FMSChanged += OnFMSChanged;
        }
        if (LStack)
        {
            Variables.FLSChanged += OnFLSChanged;
        }
    }

    void
    OnFSSChanged(object sender, System.EventArgs e)
    {
        if (Variables.FoodSStack > 0)
        {
            _Text.text = Variables.FoodSStack.ToString();
        }
        else
        {
            Variables.FSSChanged -= OnFSSChanged;
            _Text.text = null;
            _Button.interactable = false;
        }
    }
    void
    OnFMSChanged(object sender, System.EventArgs e)
    {
        if (Variables.FoodMStack > 0)
        {
            _Text.text = Variables.FoodMStack.ToString();
        }
        else
        {
            Variables.FMSChanged -= OnFMSChanged;
            _Text.text = null;
            _Button.interactable = false;
        }
    }
    void
    OnFLSChanged(object sender, System.EventArgs e)
    {
        if (Variables.FoodLStack > 0)
        {
            _Text.text = Variables.FoodLStack.ToString();
        }
        else
        {
            Variables.FLSChanged -= OnFLSChanged;
            _Text.text = null;
            _Button.interactable = false;
        }
    }

    public void
    Restart()
    {
        _Text.text = (SStack ? Variables.FoodSStack : MStack ? Variables.FoodMStack : Variables.FoodLStack).ToString();
        if (!_Button.interactable)
        {
            if (SStack)
            {
                Variables.FSSChanged += OnFSSChanged;
            }
            if (MStack)
            {
                Variables.FMSChanged += OnFMSChanged;
            }
            if (LStack)
            {
                Variables.FLSChanged += OnFLSChanged;
            }
            _Button.interactable = true;
        }
    }
}