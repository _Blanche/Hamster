﻿using UnityEngine;

public class Wheel : MonoBehaviour
{
    public Variables Variables;
    Rigidbody2D _RB2D;

    void 
    Awake()
    {
        _RB2D = GetComponent<Rigidbody2D>();
    }

    void 
    Update()
    {
        if (_RB2D.position.y <= -1f)
        {
            _RB2D.position = new Vector2(0f, .9f);
        }
        _RB2D.velocity = Vector2.down * 1.5f * Variables.WheelSpeed;
    }
}