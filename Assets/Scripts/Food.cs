using UnityEngine;

public class Food : MonoBehaviour
{
    public event System.EventHandler<int> FoodDestroyed;
    public Variables Variables;
    public HamsterWeight HamsterWeight;
    public int WeightGain;
    public int FoodID;
    Rigidbody2D _RB2D;
    Vector2 _CurrentVelocity;
    float _CurrentSpeed = 1f;

    void
    Awake()
    {
        _RB2D = GetComponent<Rigidbody2D>();    
    }

    void 
    OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            HamsterWeight.Value += WeightGain;
        }
        if (FoodDestroyed != null) { FoodDestroyed(this, FoodID); }
    }

    void 
    FixedUpdate()
    {
        _CurrentVelocity = Vector2.down * _CurrentSpeed * Variables.WheelSpeed;
        _RB2D.velocity = _CurrentVelocity;
    }
}
