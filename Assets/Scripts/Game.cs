using UnityEngine;

public class Game : MonoBehaviour
{
    public Variables Variables;
    public HamsterWeight HamsterWeight;
    public Transform[] SpawnPoints;
    public GameObject[] FoodPrefabs;
    public GameObject[] CursorPrefabs;
    public GameObject HamsterPrefab;
    public Transform LeftBorder;
    public Transform RightBorder;

    GameObject _CurrentCursor;
    bool _FoodSelected;
    int _IndexToCreate;
    bool[] FoodsAvailable;

    public float _TimeBeforeReduction = 60f;
    float _CurrentTimeReduction;
    public float _TimeBeforeFood = 4f;
    float _CurrentTimeFood;
    int _SpawnIndex;
    int _FoodIndex;
    
    void 
    Awake()
    {
        Variables.Clock = new Clock();
        Variables.Foods = new Food[12];
        FoodsAvailable = new bool[12];
        for (int i = 0; i < FoodsAvailable.Length; i++)
        {
            FoodsAvailable[i] = true;
        }
    }

    void 
    Start()
    {
        Variables.Clock.Start();
    }

    void 
    Update()
    {
        if (_FoodSelected)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 SpawnPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (SpawnPoint.x > LeftBorder.position.x && SpawnPoint.x < RightBorder.position.x)
                {
                    RaycastHit2D rayinfo = Physics2D.Raycast(SpawnPoint, Vector2.zero);
                    if (!rayinfo)
                    {
                        _FoodSelected = false;
                        Destroy(_CurrentCursor);
                        Cursor.visible = true;
                        Food newfood = GameObject.Instantiate(FoodPrefabs[_IndexToCreate], SpawnPoint, Quaternion.identity).GetComponent<Food>();
                        AddFood(newfood);
                    }
                }
            }
        }
    }

    void 
    FixedUpdate()
    {
        Variables.Clock.Update();
        if (_CurrentTimeReduction < _TimeBeforeReduction)
        {
            _CurrentTimeReduction += Time.deltaTime;
        }
        else
        {
            _CurrentTimeReduction = 0f;
            _TimeBeforeFood -= .5f;
            _TimeBeforeFood = _TimeBeforeFood > 1f ? _TimeBeforeFood : 1f;
            HamsterWeight.LossRate++;
        }
        if (_CurrentTimeFood < _TimeBeforeFood)
        {
            _CurrentTimeFood += Time.deltaTime;
        }
        else
        {
            _CurrentTimeFood = 0f;
            Variables.WheelSpeed += .01f;
            SpawnFood();
        }
    }

    void
    SpawnFood()
    {
        int i = Random.Range(0, SpawnPoints.Length);
        int j = Random.Range(0, FoodPrefabs.Length);
        while (i == _SpawnIndex)
        {
            i = Random.Range(0, SpawnPoints.Length);
        }
        while (j == _FoodIndex)
        {
            j = Random.Range(0, FoodPrefabs.Length);
        }
        _SpawnIndex = i;
        _FoodIndex = j;
        Food newfood = GameObject.Instantiate(FoodPrefabs[_FoodIndex], SpawnPoints[_SpawnIndex].position, Quaternion.identity).GetComponent<Food>();
        AddFood(newfood);
    }

    public void
    PutSFood()
    {
        if (Variables.FoodSStack > 0)
        {
            Variables.FoodSStack--;
            Cursor.visible = false;
            _CurrentCursor = GameObject.Instantiate(CursorPrefabs[0], Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
            _FoodSelected = true;
            _IndexToCreate = 0;
        }
    }

    public void
    PutMFood()
    {
        if (Variables.FoodMStack > 0)
        {
            Variables.FoodMStack--;
            Cursor.visible = false;
            _CurrentCursor = GameObject.Instantiate(CursorPrefabs[1], Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
            _FoodSelected = true;
            _IndexToCreate = 1;
        }
    }

    public void
    PutLFood()
    {
        if (Variables.FoodLStack > 0)
        {
            Variables.FoodLStack--;
            Cursor.visible = false;
            _CurrentCursor = GameObject.Instantiate(CursorPrefabs[2], Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
            _FoodSelected = true;
            _IndexToCreate = 2;
        }
    }

    void
    AddFood(Food toadd)
    {
        int foodID = -1;
        for (int i = 0; i < FoodsAvailable.Length; i++)
        {
            if (FoodsAvailable[i])
            {
                foodID = i;
                FoodsAvailable[i] = false;
                break;
            }
        }
        toadd.FoodDestroyed += OnFoodDestroyed;
        toadd.FoodID = foodID;
        Variables.Foods[foodID] = toadd;
    }

    void 
    OnFoodDestroyed(object sender, int e)
    {
        RemoveFood(e);
    }

    void
    RemoveFood(int ID)
    {
        Variables.Foods[ID].FoodDestroyed -= OnFoodDestroyed;
        Destroy(Variables.Foods[ID].gameObject);
        Variables.Foods[ID] = null;
        FoodsAvailable[ID] = true;
    }

    public void
    Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void
    Restart()
    {
        if (_FoodSelected)
        {
            _FoodSelected = false;
            Destroy(_CurrentCursor);
            Cursor.visible = true;
        }
        for (int i = 0; i < FoodsAvailable.Length; i++)
        {
            if (!FoodsAvailable[i])
            {
                Variables.Foods[i].FoodDestroyed -= OnFoodDestroyed;
                Destroy(Variables.Foods[i].gameObject);
                Variables.Foods[i] = null;
                FoodsAvailable[i] = true;
            }
        }
        _CurrentTimeFood = 0f;
        _CurrentTimeReduction = 0f;
        _TimeBeforeReduction = 45f;
        _TimeBeforeFood = 4f;
        Variables.Clock.Reset();
        Variables.Reset();
        HamsterWeight.Reset();
        GameObject.Instantiate(HamsterPrefab, new Vector2(0f, 1f), Quaternion.identity);
        Variables.Clock.Start();
    }
}
